# WeatherDemo

Initial requirements:

The purpose of the task is to ensure that you have knowledge of iOS ecosystem and you're familiar with modern mobile application architectures. The application should me weather for entered city. The task includes user stories below:
1. As a user when I open an application I want to see a list view and search bar at the top of the screen. 
2. As a user when I enter the title of the city (for example, Praha or Brno) I would like to have the list with current weather for entered city (or cities) updated from Internet. Use any open API to get weather data.
3. As a user when I see a list with the weather for entered city (or cities) I'd like to see an icon, wind speed, humidity and the temperature in Celsius.
4. As a developer I would like to have unit-tests written.

Feel free to use frameworks you like. We like MVP, MVVM mobile architectures. We welcome the usage of dependency injection principle and reactive extensions as it simplifies the whole code a lot.

Architecture:

iOS native application, based on MvvM pattern and RxSwift.

API usage:
http://gd.geobytes.com - get cities by name
http://api.openweathermap.org - get weather of current city

What wasn’t implemented from initial scope:
- Unit-tests
- Retrieve weather for several cities simultaneously.

