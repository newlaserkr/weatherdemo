//
//  CitiesModel.swift
//  WeatherTest
//
//  Created by Vadim Osovets on 8/15/17.
//  Copyright © 2017 Vadim Osovers. All rights reserved.
//

import Foundation
import Moya
import Mapper
import Moya_ModelMapper
import RxOptional
import RxSwift

class CitiesModel {
    
    let provider: RxMoyaProvider<GeoService>
    let cityName: Observable<String>
    
    let cities: Observable<[String]>
    var citiesVar = Variable<[String]>([])
    
    let disposeBag = DisposeBag()
    
    init (provider: RxMoyaProvider<GeoService>, cityName: Observable<String>) {
        
        self.provider = provider
        self.cityName = cityName
        
        cities = citiesVar.asObservable()
        
        self.cityName.subscribe(onNext: { query in
            
            print ("Search \(query)")
            
            _ = provider.request(.cities(searchQuery: query)).subscribe { [weak self] event in
                
                guard let `self` = self else { return }
                
                switch event {
                case .next(let response):
                    // do something with the data
                    print("\(response)")
                    
                    do {
                        try _ = response.filterSuccessfulStatusCodes()
                        let data = try response.mapJSON()
                        
                        var names = [String]()
                        
                        if let items = data as? Array<String> {
                            
                            // Due to some API issue sometimes it return %s, we need to handle this case
                            if items.count == 1 && items.first == "%s" {
                                // just do not show mess from API
                                return
                            }
                            
                            for item in items {
                                print("\(item)")
                                
                                // just show full address at the moment
                                names.append(item)
                            }
                        }

                        // it is observable and will show items in table view in this case
                        self.citiesVar.value = names
                        
                        print ("\(data)")
                    }
                    catch {
                        // show an error to your user
                        print("Json parsing error")
                    }
                    
                    break
                case .error(let error):
                    print("\(error)")
                    break
                default:
                    break
                }
            }
        })
            .addDisposableTo(disposeBag)
    }
}
