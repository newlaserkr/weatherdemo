//
//  Weather.swift
//  WeatherTest
//
//  Created by Vadim Osovets on 8/15/17.
//  Copyright © 2017 Vadim Osovers. All rights reserved.
//

import Foundation
import Mapper

struct WeatherWind: Mappable {
    
    let speed: Double
    
    init(map: Mapper) throws {
        try speed = map.from("speed")
    }
}

struct WeatherDescription: Mappable {

    let main: String
    let description: String
    let icon: String
    
    init(map: Mapper) throws {
        try main = map.from("main")
        try description = map.from("description")
        try icon = map.from("icon")
    }
}

struct WeatherMain: Mappable {
    
    let temp: Double
    let humidity: Int
    
    init(map: Mapper) throws {
        try temp = map.from("temp")
        try humidity = map.from("humidity")
    }
}

struct Weather: Mappable {
    
    let elements: Array<WeatherDescription>
    let main: WeatherMain
    let wind: WeatherWind
    let timeDate: String
    
    init(map: Mapper) throws {
        try main = map.from("main")
        try elements = map.from("weather")
        try wind = map.from("wind")
        try timeDate = map.from("dt_txt")
    }
}
