//
//  WeatherService.swift
//  
//
//  Created by Vadim Osovets on 8/15/17.
//
//

import Foundation
import Moya

let openWeatherDataKey = "42b591ce67e9b7074dd97c5c0cbb7ad5"

enum WeatherService {
    case current(city: String)
}

// MARK: - TargetType Protocol Implementation
extension WeatherService: TargetType {
    
    //api.openweathermap.org
    //var baseURL: URL { return URL(string: "http://samples.openweathermap.org")! }
    var baseURL: URL { return URL(string: "http://api.openweathermap.org")! }
    
    var path: String {
        switch self {
        case .current( _):
            return "/data/2.5/forecast"
        }
    }
    var method: Moya.Method {
        switch self {
        case .current:
            return .get
        }
    }
    var parameters: [String: Any]? {
        switch self {
        case .current(let city):
            return ["q": city, "appid": openWeatherDataKey, "units": "metric"]
        }
    }
    var parameterEncoding: ParameterEncoding {
        switch self {
        case .current:
            return URLEncoding.default
        }
    }
    
    var sampleData: Data {
        return "".utf8Encoded
    }
    
    var task: Task {
        switch self {
        case .current:
            return .request
        }
    }
    var headers: [String: String]? {
        return ["Content-type": "application/json"]
    }
}
// MARK: - Helpers
private extension String {
    var urlEscaped: String {
        return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
    
    var utf8Encoded: Data {
        return self.data(using: .utf8)!
    }
}

enum WeatherIconService {
    case icon(name: String)
}

// MARK: - TargetType Protocol Implementation
extension WeatherIconService: TargetType {
    var baseURL: URL { return URL(string: "http://openweathermap.org")! }
    
    var path: String {
        switch self {
        case .icon(let name):
            return "/img/w/" + name + ".png"
        }
    }
    var method: Moya.Method {
        switch self {
        case .icon:
            return .get
        }
    }
    var parameters: [String: Any]? {
        switch self {
        case .icon( _):
            return [:]
        }
    }
    var parameterEncoding: ParameterEncoding {
        switch self {
        case .icon:
            return URLEncoding.default
        }
    }
    
    var sampleData: Data {
        return "".utf8Encoded
    }
    
    var task: Task {
        switch self {
        case .icon:
            return .request
        }
    }
    var headers: [String: String]? {
        return [:]
    }
}
