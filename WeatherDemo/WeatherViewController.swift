//
//  WeatherViewController.swift
//  WeatherDemo
//
//  Created by Vadim Osovets on 8/16/17.
//  Copyright © 2017 Vadim Osovers. All rights reserved.
//

import UIKit

import RxSwift
import Moya
import RxCocoa

import Swinject

class WeatherViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var cities: [String] = []
    
    var provider: RxMoyaProvider<WeatherService>!
    var weatherModel: WeatherModel!
    
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupRx()
    }
    
    func setupRx() {
        
        provider = RxMoyaProvider<WeatherService>()
        weatherModel = WeatherModel(provider: provider, cities: cities)
        
        weatherModel.weather.bindTo(tableView.rx.items){ (tableView, row, item) in
            let cell: WeatherTableViewCell = tableView.dequeueReusableCell(withIdentifier: "weatherCell", for: IndexPath(row: row, section: 0)) as! WeatherTableViewCell
            
                cell.configure(weather: self.weatherModel.weatherVar.value[row])
            
                return cell
            }
            .addDisposableTo(disposeBag)
        
    }
}
